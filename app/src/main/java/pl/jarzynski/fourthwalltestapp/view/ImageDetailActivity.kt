package pl.jarzynski.fourthwalltestapp.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.image_detail_activity.*
import pl.jarzynski.fourthwalltestapp.R

class ImageDetailActivity: Activity() {

    companion object {
        val IMAGE_URL = "IMAGE_URL"
        val AUTHOR = "AUTHOR"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.image_detail_activity)

        val imageUrl = intent.extras?.getString(IMAGE_URL)

        authorTextView.apply {
            text = intent.extras?.getString(AUTHOR) ?: "Empty"
        }

        shareButton.setOnClickListener {
            val sendIntent: Intent = Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_SUBJECT, R.string.app_name)
                val shareImage = "\nHi, look what picture I have found: \n\n $imageUrl"
                putExtra(Intent.EXTRA_TEXT, shareImage)
                type = "text/plain"
            }

            val shareIntent = Intent.createChooser(sendIntent, resources.getString(R.string.select_how_to_share))
            startActivity(shareIntent)
        }

        val picasso = Picasso.get()
        picasso.isLoggingEnabled = true
        picasso
            .load(imageUrl)
            .placeholder(R.drawable.ic_cached_black_36dp)
            .fit()
            .centerInside()
            .into(imageView, object :
                Callback {
                override fun onSuccess() {
                    Log.d("TAG", "success")
                }

                override fun onError(e: Exception?) {
                    Log.d("TAG", "error")
                }
            })
    }
}