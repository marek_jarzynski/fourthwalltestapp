package pl.jarzynski.fourthwalltestapp.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.grid_activity.*
import pl.jarzynski.fourthwalltestapp.R

class GridActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.grid_activity)

        gridView.apply {
            supportFragmentManager.beginTransaction().add(R.id.gridView,
                GridFragment()
            ).commitNow()
        }
    }
}