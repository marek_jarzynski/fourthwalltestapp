package pl.jarzynski.fourthwalltestapp.view

import android.app.AlertDialog
import android.content.res.Configuration
import android.graphics.Rect
import android.os.Bundle
import android.view.*
import android.widget.AbsListView
import android.widget.GridView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import io.reactivex.disposables.CompositeDisposable
import pl.jarzynski.fourthwalltestapp.R
import pl.jarzynski.fourthwalltestapp.adapter.ImageAdapter
import pl.jarzynski.fourthwalltestapp.api.ImageResponse
import pl.jarzynski.fourthwalltestapp.viewmodel.ImageViewModel
import kotlin.math.floor

class GridFragment : Fragment() {
    private var imageThumbSize = 0
    private var pageCounter = 1

    lateinit var adapter: ImageAdapter
    lateinit var imageViewModel: ImageViewModel
    lateinit var disposable: CompositeDisposable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)

        disposable = CompositeDisposable()

        imageViewModel = ViewModelProvider(requireActivity()).get(ImageViewModel::class.java)
        imageViewModel.loadImages(pageCounter, disposable)
        imageThumbSize = resources.getDimensionPixelSize(R.dimen.image_thumbnail_size)

        adapter = ImageAdapter(activity)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        getAllImages()

        val gridView = GridView(context)
        gridView.columnWidth = resources.getDimension(R.dimen.image_thumbnail_size).toInt()
        gridView.horizontalSpacing =
            resources.getDimension(R.dimen.image_thumbnail_spacing).toInt()
        gridView.verticalSpacing = resources.getDimension(R.dimen.image_thumbnail_spacing).toInt()
        gridView.numColumns = GridView.AUTO_FIT
        gridView.gravity = Gravity.CENTER
        gridView.isDrawSelectorOnTop = true
        gridView.stretchMode = GridView.STRETCH_COLUMN_WIDTH
        gridView.layoutParams = AbsListView.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.MATCH_PARENT
        )
        gridView.adapter = adapter
        gridView.viewTreeObserver.addOnGlobalLayoutListener(
            object : ViewTreeObserver.OnGlobalLayoutListener {
                override fun onGlobalLayout() {
                    if (adapter.numColumns == 0) {
                        val numColumns =
                            floor(gridView.width / imageThumbSize.toDouble()).toInt()
                        if (numColumns > 0) {
                            val columnWidth = gridView.width / numColumns
                            adapter.numColumns = numColumns
                            adapter.itemHeight = columnWidth
                            gridView.viewTreeObserver.removeOnGlobalLayoutListener(this)
                        }
                    }
                }
            })
        gridView.setOnScrollListener(object : AbsListView.OnScrollListener {
            override fun onScroll(
                view: AbsListView?,
                firstVisibleItem: Int,
                visibleItemCount: Int,
                totalItemCount: Int
            ) {
                if (visibleItemCount > 0 && totalItemCount > 0) {
                    if (firstVisibleItem + visibleItemCount >= pageCounter * 30) {
                        pageCounter++
                        imageViewModel.loadImages(pageCounter, disposable)
                    }
                }
            }

            override fun onScrollStateChanged(view: AbsListView?, state: Int) {
                //
            }
        })
        return gridView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        builder.setCancelable(false)
        builder.setView(R.layout.loading_dialog)
        val dialog: AlertDialog = builder.create()

        disposable.add(
            imageViewModel.startDownload.subscribe { result ->
                if (result) {
                    dialog.show()
                    imageViewModel.startDownload.onNext(false)
                }
            })

        disposable.add(
            imageViewModel.endDownload.subscribe { result ->
                if (result) {
                    dialog.dismiss()
                    imageViewModel.endDownload.onNext(false)
                }
            })

        disposable.add(
            imageViewModel.internetMissing.subscribe { result ->
                if (result) {
                    Toast.makeText(
                        requireActivity(),
                        R.string.no_internet_connection,
                        Toast.LENGTH_LONG
                    ).show()
                    imageViewModel.internetMissing.onNext(false)
                }
            })

        disposable.add(
            imageViewModel.errorMsg.subscribe { result ->
                if (result.isNotEmpty()) {
                    Toast.makeText(
                        requireActivity(),
                        result,
                        Toast.LENGTH_LONG
                    ).show()
                    imageViewModel.errorMsg.onNext("")
                }
            })
    }

    private fun getAllImages() {
        imageViewModel.getImages()
            .observe(requireActivity(), Observer<MutableList<ImageResponse>> { images ->
                run {
                    adapter.setImagesList(images)
                    adapter.notifyDataSetChanged()
                }
            })
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        activity?.window?.let { win ->
            val rect = Rect()
            win.decorView.getWindowVisibleDisplayFrame(rect)
            val height = rect.width()

            if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT || newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                val numColumns = floor(height / imageThumbSize.toDouble()).toInt()
                if (numColumns > 0) {
                    val columnHeight = height / numColumns
                    adapter.numColumns = numColumns
                    adapter.itemHeight = columnHeight
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        disposable.dispose()
    }
}