package pl.jarzynski.fourthwalltestapp.adapter

import android.app.Activity
import android.app.ActivityOptions
import android.content.Intent
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.AbsListView
import android.widget.BaseAdapter
import android.widget.ImageView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import pl.jarzynski.fourthwalltestapp.R
import pl.jarzynski.fourthwalltestapp.api.ImageResponse
import pl.jarzynski.fourthwalltestapp.view.ImageDetailActivity

class ImageAdapter(private val activity: Activity?) : BaseAdapter() {
    private var mItemHeight = 0
    var numColumns = 0
    private var images = mutableListOf<ImageResponse>()
    private var mImageViewLayoutParams: AbsListView.LayoutParams
    override fun getCount(): Int {
        // If columns have yet to be determined, return no items
        return if (numColumns == 0) {
            0
        } else images.size
    }

    fun setImagesList(imagesList: List<ImageResponse>?) {
        this.images = imagesList?.toMutableList() ?: mutableListOf()
        notifyDataSetChanged()
    }

    override fun getItem(position: Int): Any {
        return images[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun hasStableIds(): Boolean {
        return true
    }

    override fun getView(
        position: Int,
        convertView: View?,
        container: ViewGroup
    ): View {
        val image = images[position]
        mImageViewLayoutParams.height = itemHeight
        mImageViewLayoutParams.width = itemHeight

        val imageView: ImageView
        if (convertView == null) {
            imageView = ImageView(activity)
            imageView.scaleType = ImageView.ScaleType.CENTER_CROP
            imageView.layoutParams = mImageViewLayoutParams
        } else {
            imageView = convertView as ImageView
        }

        // Check the height matches our calculated column width
        if (imageView.layoutParams.height != mItemHeight) {
            imageView.layoutParams = mImageViewLayoutParams
        }

        val picasso = Picasso.get()
        picasso.isLoggingEnabled = true
        picasso
            .load(image.download_url)
            .placeholder(R.drawable.ic_cached_black_36dp)
            .fit()
            .centerInside()
            .into(imageView, object :
                Callback {
                override fun onSuccess() {
                    Log.d("TAG", "success")
                }

                override fun onError(e: Exception?) {
                    Log.d("TAG", "error")
                }
            })

        imageView.setOnClickListener {
            val i = Intent(activity, ImageDetailActivity::class.java)
            i.putExtra(ImageDetailActivity.IMAGE_URL, image.download_url)
            i.putExtra(ImageDetailActivity.AUTHOR, image.author)
            val options =
                ActivityOptions.makeScaleUpAnimation(imageView, 0, 0, imageView.width, imageView.height)
            activity!!.startActivity(i, options.toBundle())
        }
        return imageView
    }

    /**
     * Sets the item height. Useful for when we know the column width so the
     * height can be set
     * to match.
     *
     * @param height
     */
    var itemHeight: Int
        get() = mItemHeight
        set(height) {
            if (height == mItemHeight) {
                return
            }
            mItemHeight = height
            mImageViewLayoutParams = AbsListView.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
            )
            notifyDataSetChanged()
        }

    init {
        mImageViewLayoutParams = AbsListView.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.MATCH_PARENT
        )
    }
}