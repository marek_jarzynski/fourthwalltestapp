package pl.jarzynski.fourthwalltestapp.api

import java.io.IOException

class NoConnectivityException : IOException() {
    override val message: String
        get() = "No Internet Connection"
}
