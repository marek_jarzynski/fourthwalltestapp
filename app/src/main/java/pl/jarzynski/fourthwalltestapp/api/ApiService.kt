package pl.jarzynski.fourthwalltestapp.api

import android.content.Context
import android.net.ConnectivityManager
import com.google.gson.GsonBuilder
import io.reactivex.Observable
import okhttp3.OkHttpClient
import pl.jarzynski.fourthwalltestapp.MyApp
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("/v2/list")
    fun getImages(@Query("page") page: Int): Observable<List<ImageResponse>>

    @GET("/notification")
    fun getImage(@Query("image") id: Int): Observable<ImageResponse>

    companion object {
        private const val instance = "https://picsum.photos"

        fun create(): ApiService {
            val okHttp = OkHttpClient.Builder()
                .addInterceptor(NetworkConnectionInterceptor())

            val gson = GsonBuilder()
                .setLenient()
                .create()

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(instance).client(okHttp.build())
                .build()

            return retrofit.create(ApiService::class.java)
        }

        val isConnected: Boolean
            get() {
                val connectivityManager =
                    MyApp.context?.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val netInfo = connectivityManager.activeNetworkInfo
                return netInfo != null && netInfo.isConnected
            }
    }
}