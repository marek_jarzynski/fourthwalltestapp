package pl.jarzynski.fourthwalltestapp.api

import java.io.IOException

class DownloadException(var code: Int, var type: String) : IOException() {

}
