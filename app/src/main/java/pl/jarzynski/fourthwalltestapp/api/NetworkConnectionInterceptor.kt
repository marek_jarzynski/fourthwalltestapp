package pl.jarzynski.fourthwalltestapp.api

import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException


class NetworkConnectionInterceptor() :
    Interceptor {
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
        if (!ApiService.isConnected) {
            throw NoConnectivityException()
        }
        val builder = chain.request().newBuilder()
        val response = chain.proceed(builder.build())
        when (response.code()) {
            in 400..499 -> {
                throw DownloadException(response.code(), "client")
            }
            in 500..599 -> {
                throw DownloadException(response.code(), "server")
            }
        }
        return response
    }
}