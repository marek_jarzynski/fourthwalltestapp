package pl.jarzynski.fourthwalltestapp.api

data class ImageResponse(val id: Int, val author: String, val width: Int, val height: Int, val download_url: String)
