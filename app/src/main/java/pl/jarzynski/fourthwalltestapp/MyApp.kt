package pl.jarzynski.fourthwalltestapp

import android.app.Application
import android.content.Context
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ProcessLifecycleOwner

class MyApp : Application(), LifecycleObserver {
    override fun onCreate() {
        super.onCreate()
        context = this
        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
    }

    companion object {
        var context: Context? = null
            private set
    }
}