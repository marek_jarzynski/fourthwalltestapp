package pl.jarzynski.fourthwalltestapp.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import pl.jarzynski.fourthwalltestapp.api.ApiService
import pl.jarzynski.fourthwalltestapp.api.DownloadException
import pl.jarzynski.fourthwalltestapp.api.ImageResponse
import pl.jarzynski.fourthwalltestapp.api.NoConnectivityException

class ImageViewModel : ViewModel() {
    private var images = ArrayList<ImageResponse>()
    private var imagesLiveData = MutableLiveData<MutableList<ImageResponse>>()

    var startDownload: BehaviorSubject<Boolean> = BehaviorSubject.createDefault(false)
    var endDownload: BehaviorSubject<Boolean> = BehaviorSubject.createDefault(false)
    var internetMissing: BehaviorSubject<Boolean> = BehaviorSubject.createDefault(false)
    var errorMsg: BehaviorSubject<String> = BehaviorSubject.createDefault("")

    fun loadImages(page: Int, disposable: CompositeDisposable) {
        val instanceApi = ApiService.create()
        startDownload.onNext(true)

        disposable.add(instanceApi.getImages(page)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    Log.d("images", result.toString())
                    result.forEach { image ->
                        if (!images.any { it.id == image.id }) {
                            images.add(image)
                        }
                    }
                    imagesLiveData.value = images
                    endDownload.onNext(true)
                },
                { error ->
                    Log.d("error", error.toString())
                    if (error is NoConnectivityException) {
                        internetMissing.onNext(true)
                    }
                    if (error is DownloadException) {
                        errorMsg.onNext("Request sent back ${error.code} code, it is a ${error.type} error")
                    }
                    endDownload.onNext(true)
                }
            )
        )
    }

    fun getImages(): LiveData<MutableList<ImageResponse>> {
        return imagesLiveData
    }
}