# FourthWallTestApp

Description of FourthWallTestApp. To create the app I decided to choose MVVM architecture concept. View model will handle all network communication and provide ready list for grid.
Whole app has two activities:

  - GridActivity - main activity with a grid view showing images
  - ImageDetailActivity - detail activity for showing selected image
 
I decided to use a combination of Retrofit + RxJava/RxAndroid to retrieve data from network. This enabled to quite simple handle such edge cases as no internet connection or error messages while downloading.
Function for getting images is universal and you have to provide page number to get proper set of images. I decided no to call for single image as when getting whole list, we already get a URL for images so I just share it with details Activity using intent.
User can share URL to image after clicking share button in details view.

For standard sized smartphone 2 columns will shown in grid, for larger devices such as tablets more. No of columns now depends on screen size of device. When orientation is changed then it is recalculated to fill the space better.

Image caching is done on Picasso library side.

### Used libraries

FourthWallTestApp uses a number of open source libraries to work:

* [Picasso](https://square.github.io/picasso/) - Image downloading and caching library
* [Retrofit](https://square.github.io/retrofit/) - HTTP client
* [GSON](https://github.com/google/gson) - JSON parsing library
* [RxAndroid](https://github.com/ReactiveX/RxAndroid) - Reactive extensions for Android
* [RxJava](https://github.com/ReactiveX/RxJava) - Reactive extensions for java

License
----

MIT
